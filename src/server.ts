import App from './app'
import ParkingController from './controller/parking.controller';
import PlotController from './controller/plot.controller';
import PSlotController from './controller/pslot.controller';
import Controller from './interfaces/controller.interface';
const port = process.env.PORT || "3003"


const controllers:Controller[] = [new PlotController(),new ParkingController(),new PSlotController() ];
const app = new App(controllers)
//app.listen(port,(err)=>{
  // app.listen(port,()=>{
  // //   if(err){
  // //       return console.log(err);
  // //   }
  //   return console.log(`server is running  on ${port}`);
  // })
app.listen(port);