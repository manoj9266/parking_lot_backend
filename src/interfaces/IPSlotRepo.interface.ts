export interface IPSlotRepo extends Repo<any>{
    getSlot(lot_id:number): Promise<any>;
    create(lot_id:number,slot_count:number): Promise<any>;
    update(wheer_data:any,status:number): Promise<any>
}