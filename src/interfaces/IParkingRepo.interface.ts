export interface IParkingRepo extends Repo<any>{
    create(slot_id:number): Promise<any>;
    unpark(parking_id:number,charge_per_hour:number,num_hours:number,slot_id:number): Promise<any> ;
    getSummary(start_time:string,end_time:string): Promise<any> ;
}