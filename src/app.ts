import express,{Application,Request,Response,NextFunction} from "express";
import './database';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import Controller from './interfaces/controller.interface';
const cors = require('cors');
import * as config from './config/config.json';
class App{
     public app:Application;
     constructor(controllers:Controller[]){
        this.app = express();
        this.init();
        this.mountRoutes(controllers);        
    }
    private init(){
        const whitelist = config.cors_urls?config.cors_urls:[];
        console.log(whitelist);
        
        const corsOptions = {
          origin: function (origin:any, callback:any) {
            console.log("origin:"+origin);
            //if (whitelist.indexOf(origin) !== -1) {
              callback(null, true)
            // } else {
            //   callback(new Error('Not allowed by CORS'))
            // }
          }
        }
        this.app.use(cors(corsOptions));        
        this.app.use(helmet());    
        this.app.use(bodyParser.json({limit:'5mb'}));
    }
    private mountRoutes(controllers:Controller[]):void {
        controllers.forEach((controller:Controller)=>{
            this.app.use(controller.path,controller.router);

        })            
    }
    public listen(port:string){

        this.app.listen(port,()=>{              
              return console.log(`server is running  on ${port}`);
            })
    }
}
export default App;