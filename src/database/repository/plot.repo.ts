import { resolve } from 'path';
//import {sequelize,PLotModel} from  '../index';
import {sequelize} from "../index";
import PLot from '../model/plot.model'
import PSlot from '../model/pslot.model'
import {Transaction} from "sequelize";
import { IPLotRepo } from '../../interfaces/IPLotRepo.interface';

export default class PlotRepo implements IPLotRepo{

    public findOne(where_ob:any): Promise<any> {
        return new Promise<any>((resolve:Function,reject:Function)=>{
            sequelize.transaction((t:Transaction) => {
            return PLot.findOne({ where:where_ob,raw:true}).then((plot:any)=>{                
                      resolve(plot);                        
            }).catch((err:Error)=>{
                reject(err);
      
            })
            
        });        

        }) 
    }
    
    public async create(slot_count:number,charge_per_hour:number=10): Promise<any> {        
        let t = await sequelize.transaction({autocommit:false});
        try{
            const plot:any = await PLot.create({slot_count: slot_count,charge_per_hour:charge_per_hour})
            let slots = [];
                for(let i=1;i<=slot_count;i++){
                    slots.push({lot_id:plot.id,slot_number:i,status:0});
                 }
             await PSlot.bulkCreate(slots);
             t.commit();
             return plot;     
        }catch(err){
          console.log("err:"+err);
          t.rollback();          
          return false;
        }
    }
}
