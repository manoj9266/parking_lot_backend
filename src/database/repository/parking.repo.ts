import { resolve } from 'path';
import Parking from  '../model/parking.model';
import {sequelize} from '..';
import PSlot from  '../model/pslot.model';
import {DateDataType, Sequelize, Transaction,Op,QueryTypes } from "sequelize";
import moment from 'moment';
import { rejects } from 'assert';
import { IParkingRepo } from '../../interfaces/IParkingRepo.interface';
export default class ParkingRepo implements IParkingRepo{
    public findOne(where_ob:any): Promise<any> {        
            return Parking.findOne({ where:where_ob,raw:true});        
    }
    
    // public  getParkingWithSlot(id:number): Promise<any> {
    //     return Parking.findOne({ where:{id: id}})
    // }
    
    public  create(slot_id:number): Promise<any> {
        let item = {slot_id:slot_id};                
        return Parking.create(item)
    }
    public unpark(parking_id:number,charge_per_hour:number,num_hours:number,slot_id:number): Promise<any> {
        
        return new Promise<any>((resolve:Function,reject:Function)=>{
            sequelize.transaction((t:Transaction) => {                
            const updateData = {status:2,hours_count:num_hours,charge:num_hours*charge_per_hour,exit_time:moment(new Date()).utc().format('YYYY-MM-DD HH:mm:ss')};                              
             return Parking.update(updateData,{where:{id:parking_id}}).then((data:any)=>{                                
                  
                  PSlot.update({status:0},{where:{id:slot_id}}).then((data:any)=>{                                        
                    resolve(null);
                  }).catch((err:Error)=>{
                      t.rollback();
                      reject(err);
                  })                
            }).catch((err:Error)=>{
                t.rollback();
                reject(err);
            })
            
        });        

        }) 
    }
    public  getSummary(start_time:string,end_time:string): Promise<any> {                
            const where_data = {entry_time:{[Op.gt]:start_time},exit_time:{[Op.lt]:end_time},status:2}
            return Parking.findAll({where:where_data,attributes: [
                [Sequelize.fn('sum', Sequelize.col('charge')), 'total_amout'],
                [Sequelize.fn('count', Sequelize.col('status')), 'total_parking'],
                [Sequelize.fn('sum', Sequelize.col('hours_count')), 'total_hours']
            ],group:"status"});                             
    }
}
