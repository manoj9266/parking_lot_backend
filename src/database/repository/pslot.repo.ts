import { resolve } from 'path';
import {sequelize} from  '../index';
import PSlot from '../model/pslot.model';
import {Transaction} from "sequelize";
import { IPSlotRepo } from '../../interfaces/IPSlotRepo.interface';

export default class PslotRepo implements IPSlotRepo{
    public  getSlot(lot_id:number): Promise<any> {
        return new Promise<any>((resolve:Function,reject:Function)=>{
            sequelize.transaction((t:Transaction) => {
            //return PSlotModel.findOne({ where:{fk_lot_id:lot_id,status:0},order:[['slot_number','ASC']]}).then((plot:any)=>{                
                return PSlot.findOne({ where:{lot_id:lot_id,status:0},order:[['slot_number','ASC']]}).then((plot:any)=>{                
                      resolve(plot);                        
            }).catch((err:Error)=>{
                reject(err);
      
            })
            
        });        

        }) 
    }
    public findOne(where_ob:any): Promise<any> {
        return new Promise<any>((resolve:Function,reject:Function)=>{
            sequelize.transaction((t:Transaction) => {
            return PSlot.findOne({ where:where_ob,raw:true}).then((plot:any)=>{                
                      resolve(plot);                        
            }).catch((err:Error)=>{
                reject(err);
      
            })
            
        });        

        }) 
    }
    public  create(lot_id:number,slot_count:number): Promise<any> {
        console.log("plot repo create");
        return new Promise<any>((resolve:Function,reject:Function)=>{
            sequelize.transaction((t:Transaction) => {
                let items = [];
                for(let i=1;i<=slot_count;i++){
                   items.push({lot_id:lot_id,slot_number:i,status:false});
                }
            return PSlot.bulkCreate(items).then((slots:any)=>{
    
                      resolve(slots);                        
            }).catch((err:Error)=>{
                reject(err);
      
            })
            
        });        

        }) 
    }
    public  update(wheer_data:any,status:number): Promise<any> {
        
        return new Promise<any>((resolve:Function,reject:Function)=>{
            sequelize.transaction((t:Transaction) => {
               
            return PSlot.update({status:status},{where:wheer_data}).then((slots:any)=>{
    
                resolve(null);
            }).catch((err:Error)=>{
                reject(err);
      
            })
            
        });        

        }) 
    }
}
