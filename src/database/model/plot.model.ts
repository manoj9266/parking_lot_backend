const { Sequelize, DataTypes, Model } = require('sequelize');
import {sequelize} from '../index';
const PLot =  sequelize.define('Plot',{
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement:true,
    primaryKey:true
    },
  created_at: {
    type: 'TIMESTAMP',        
    defaultValue:Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  },
  slot_count: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,        
    },
  charge_per_hour:{
    type: DataTypes.TINYINT(3),
    allowNull: false,                
  }  

},{
  timestamps :false
});        
export default PLot;
//PLot.sync({}).then(()=>{console.log("plot table created")}) 
