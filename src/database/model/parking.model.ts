const { Sequelize, DataTypes, Model } = require('sequelize');
import moment from 'moment';
import PSlot from './pslot.model';
import { sequelize } from '..';

const Parking =sequelize.define('Parking',{
        id: {
          type: DataTypes.BIGINT.UNSIGNED,
          allowNull: false,
          autoIncrement:true,
          primaryKey:true
          },
          entry_time: {
            type: 'TIMESTAMP',  
            defaultValue:Sequelize.literal('CURRENT_TIMESTAMP') ,
          allowNull: false,
          },
          exit_time: {
            type: 'TIMESTAMP',  
            defaultValue:Sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull:false 
          },
          slot_id: {
            type: DataTypes.BIGINT.UNSIGNED,
            allowNull: false,          
          },      
        status:{
          type: DataTypes.TINYINT(1),   
          allowNull: false,
          defaultValue:1               //1=> parked, 2=> unparked               
        } , 
        hours_count:{
          type: DataTypes.INTEGER.UNSIGNED,     
          defaultValue:0      
        }  ,
        charge:{
          type: DataTypes.INTEGER.UNSIGNED,
          defaultValue:0                 
        }  
  
      },{timestamps :false,indexes:[
        {
          unique: false,
          fields:['entry_time']
        },
        {
          unique: false,
          fields:['exit_time']
        }
       ]
      });        

    Parking.belongsTo(PSlot,{foreignKey: 'slot_id'});
    export default Parking;