import { Sequelize, DataTypes, Model,Deferrable } from 'sequelize';
import { sequelize } from '..';
import PLot from './plot.model';
const PSlot = sequelize.define('pslot',{
        id: {
          type: DataTypes.BIGINT.UNSIGNED,
          allowNull: false,
          autoIncrement:true,
          primaryKey:true
          },
          lot_id: {//foreign key
            type: DataTypes.INTEGER,
            allowNull: false,            
            },          
        slot_number:{
          type: DataTypes.INTEGER,
          allowNull: false,                
        },
        status:{
          type: DataTypes.TINYINT,   //0 =>unparked, 1=>parked, -1 => under_maintenance(disabled);
          allowNull: false,                
        }  
  
      },{timestamps :false}
      );
//association     
PSlot.belongsTo(PLot,{foreignKey: 'lot_id',targetKey:'id'});

export default PSlot;

            
