import { Sequelize,Model } from 'sequelize';
import * as dotenv from 'dotenv';
dotenv.config();
const db_name = process.env.DB_NAME+"";
const db_host = process.env.DB_HOST;
const db_user = process.env.DB_USER+"";
const db_pass = process.env.DB_PASS;

class Database {
  private _sequelize: Sequelize;
  
  constructor(){
   this._sequelize = new Sequelize(db_name, db_user, '', {
      host: db_host,
      dialect: 'mysql'
    });
   

    this._sequelize.sync({}).then((res)=>{console.log('Connection has been established successfully.');})
    .catch((error)=>{
      console.error('Unable to connect to the database:', error);
    })
  } 
  associate(){
   //   this._models.plot.hasMany(this._models.pslot,{sourceKey: 'id',foreignKey: 'lot_id'}); 
   //   this._models.pslot.belongsTo(this._models.plot,{foreignKey: 'lot_id',targetKey:'id'});
   //   this._models.parking.belongsTo(this._models.pslot,{foreignKey: 'slot_id'});
     //this._models.parking.hasOne(this._models.pslot,{as:"pslot",constraints: false});      
  }
  
  getSequelize() {
   return this._sequelize;
   
 }
}

console.log("database index");
const database = new Database();
export const sequelize =database.getSequelize();

