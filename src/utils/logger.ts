import {transports, Logger} from "winston";
import winston from 'winston';
export const logger = winston.createLogger({
    transports: [
        new transports.Console(),
        new transports.File({ filename: 'combined.log' })
    ],
    exitOnError: false
  });
  