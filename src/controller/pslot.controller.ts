import express,{Request,Response} from 'express'
import Controller from '../interfaces/controller.interface';
import PslotRepo from '../database/repository/pslot.repo'
import {sequelize} from '../database';
class PlotController implements Controller{
  private PslotRepo = new PslotRepo();
    public router = express.Router();
    public path = "/pslot";
    constructor(){
        this.initializeRoutes();
      }
      private initializeRoutes(){   
        
        this.router.post('/maintenance',this.maintenace);
 
        this.router.get('/',this.getSLot);
       }
       private maintenace = async (req:Request,res:Response)=>{
        //const t = await sequelize.transaction();
         try{         
         const params = req.body;            
         const slot_id = +params.slot_id;
         const lot_id = +params.lot_id;                  
         const slot_number = +params.slot_number;                  
         const status = +params.status;
         if(status!=0 && status!=-1){
           return res.json({status:false,message:"invalid status"});
         }else if(!slot_id && (!lot_id || !slot_number)){
          return res.json({status:false,message:"invalid parameter"});
         }

         
         let where_ob = {};
         if(slot_id){
           where_ob = {id:slot_id};
         }else{
          where_ob = {lot_id:lot_id,slot_number:slot_number};
         }
         let pslot = await this.PslotRepo.findOne(where_ob);
         if(pslot.status==1){
           return res.json({status:false,message:"slot is occupied"});
         } 
         let plot = await this.PslotRepo.update(where_ob,status);  
         console.log("plot resp:"+JSON.stringify(plot));       
         return res.json({status:true,message:"success"});
        }catch(err){
          //t.rollback();
          console.log(err);
          return res.status(400).send("something went wrong!!");
        }
       }
       private getSLot = async (req:Request,res:Response)=>{
        try{
          const params = req.query;            
          const slot_id = params.slot_id?+params.slot_id:0;
          const lot_id = params.lot_id?+params.lot_id:0;                  
          const slot_number = params.slot_number?+params.slot_number:0;                            
           if(!slot_id && (!lot_id || !slot_number)){
           return res.json({status:false,message:"invalid parameter"});
          }
 
          
          let where_ob = {};
          if(slot_id){
            where_ob = {id:slot_id};
          }else{
           where_ob = {lot_id:lot_id,slot_number:slot_number};
          }
          let pslot = await this.PslotRepo.findOne(where_ob);
          res.send(pslot);
          }catch(err){
            console.log(err);
            res.status(400).send("something went wrong!!");
          }
       }        
      
}
export default PlotController;