import express,{Request,Response} from 'express'
import Controller from '../interfaces/controller.interface';
import PlotRepo from '../database/repository/plot.repo'
import PslotRepo from '../database/repository/pslot.repo'
import {sequelize} from '../database';
class PlotController implements Controller{
   private PlotRepo = new PlotRepo();
   private PslotRepo = new PslotRepo();
    public router = express.Router();
    public path = "/plot";
    constructor(){
        this.initializeRoutes();
      }
      private initializeRoutes(){   
        
        this.router.post('/',this.createLot);
 
        this.router.get('/',this.getLot);
       }
       private createLot = async (req:Request,res:Response)=>{
         try{       
          const params = req.body;            
         const slot_count = +params.slot_count;         
         if(!slot_count){
           return res.status(500).json({message:"slot required"});
         }
         
         let plot = await this.PlotRepo.create(slot_count);           
                  
              return res.json(plot);
        }catch(err){
          console.log(err);
          return res.status(400).send("something went wrong!!");
        }
       }
       private getLot = async (req:Request,res:Response)=>{
        try{
        
          res.send({status:true});
          }catch(err){
            console.log(err);
            res.status(400).send("something went wrong!!");
          }
       }        
      
}
export default PlotController;