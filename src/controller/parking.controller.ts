import express,{Request,Response} from 'express'
import Controller from '../interfaces/controller.interface';
import PlotRepo from '../database/repository/plot.repo'
import PslotRepo from '../database/repository/pslot.repo'
import ParkingRepo from '../database/repository/parking.repo'
import moment from 'moment';
moment().format();

class PlotController implements Controller{
     private PslotRepo = new PslotRepo();
     private PlotRepo = new PlotRepo();
     private ParkingRepo = new ParkingRepo();
    public router = express.Router();
    public path = "/park";
    constructor(){
        this.initializeRoutes();
      }
      private initializeRoutes(){   
        
        this.router.post('/',this.park);
        this.router.post('/unpark',this.unpark); 
        this.router.get('/',this.getParking);
        this.router.get('/summary',this.getSummary);
       }
       private park = async (req:Request,res:Response)=>{
         try{
         
          const params = req.body;                     
         const lot_id = +params.lot_id;
         if(!lot_id){
           return res.json({status:false,message:"lot_id required"});
         }
         let slot = await this.PslotRepo.getSlot(lot_id);
         if(!slot){
           return res.json({status:false,message:"no slot available"});
         }
          let parking = await this.ParkingRepo.create(slot.id);                  
          await this.PslotRepo.update({id:slot.id},1);
          parking.slot_number = slot.slot_number;
         
        return res.json(parking);
        }catch(err){
          console.log(err);
          return res.status(400).send("something went wrong!!");
        }
       }
       private unpark = async (req:Request,res:Response)=>{
        try{
        
        const params = req.body;                    
        const parking_id = +params.parking_id;
        
        if(!parking_id){
          return res.json({status:false,message:"parking_id required"});
        }
        
        const parking = await this.ParkingRepo.findOne({id:parking_id});
        if(!parking){
          return res.json({status:false,message:"parking not found"});
        }else  
        if(parking.status==2){
        return res.json({status:false,message:"already unparked"});
        }          
        
        const pslot = await this.PslotRepo.findOne({id:parking.slot_id});         
        const plot = await this.PlotRepo.findOne({id:pslot.lot_id});
        
        parking.slot_number = pslot.slot_number;
        parking.charge_per_hour = plot.charge_per_hour;        
        const entry_time = moment(parking.entry_time) ;
        const now = moment(new Date());
        const duration  = moment.duration(now.diff(entry_time));
        const num_hours = Math.ceil(duration.asHours());        
        
        await this.ParkingRepo.unpark(parking_id,parking.charge_per_hour,num_hours,parking.slot_id);                        
       return res.json({status:true,message:"success"});
       }catch(err){
         console.log(err);
         return res.status(400).send("something went wrong!!");
       }
      }
       private getParking = async (req:Request,res:Response)=>{
        try{
          const params = req.query;                    
          const parking_id = params.parking_id?+params.parking_id:0;
          
          if(!parking_id){
            return res.json({status:false,message:"parking_id required"});
          }
          
          const parking = await this.ParkingRepo.findOne({id:parking_id});
          
          return res.json(parking);
                    
          }catch(err){
            console.log(err);
            res.status(400).send("something went wrong!!");
          }
       }

       private getSummary = async (req:Request,res:Response)=>{
        try{
          const params = req.query;                    
          const start_time = moment(new Date()).subtract(1,'day').format('YYYY-MM-DD HH:mm:ss');
          const end_time = moment(new Date()).format('YYYY-MM-DD HH:mm::ss');          
          console.log({start_time,end_time});
          let resp = await this.ParkingRepo.getSummary(start_time,end_time);
          return res.json(resp);
                    
          }catch(err){
            console.log(err);
            res.status(400).send("something went wrong!!");
          }
       }
}
export default PlotController;